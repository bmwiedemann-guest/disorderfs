#!/bin/sh -e
verbose=0
[ "$1" = --verbose ] && verbose=1 && shift

dir=$1
for prog in `find $dir -type f -perm -1 | LC_ALL=C sort` ; do
    [ "$verbose" = 0 ] || echo "run-parts: executing $prog" >&2
    $prog
done
